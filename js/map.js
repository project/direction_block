/**
 * @file
 * Javascript function for direction_block module.
 */

(function ($) {
  Drupal.behaviors.direction_block = {
    attach: function(context, settings) {
      var originLat = Number(Drupal.settings.directionBlock['originLatitude']);
      originLat = (originLat).toFixed(2);
      var originLon = Number(Drupal.settings.directionBlock['originLongitude']);
      originLon = (originLon).toFixed(2);
      var endLat = Number(Drupal.settings.directionBlock['endLatitude']);
      endLat = (endLat).toFixed(2);
      var endLon = Number(Drupal.settings.directionBlock['endLongitude']);
      endLon = (endLon).toFixed(2);
      $(function() {
        new Maplace({
          locations: [{
            lat: originLat,
            lon: originLon,
            title: 'Title A1',
            html: '<h3>Content A1</h3>',
            zoom: 8,
            icon: 'http://www.google.com/mapfiles/markerA.png'
          },
            {
              lat: endLat,
              lon: endLon,
              title: 'Title B1',
              html: '<h3>Content B1</h3>',
              show_infowindow: false
            }
          ],
          map_div: '#gmap',
          generate_controls: false,
          show_markers: false,
          type: 'directions',
          draggable: true,
          directions_panel: '#route',
          afterRoute: function(distance) {
            $('#km').text(': ' + (distance / 1000) + 'km');
          }
        }).Load();
      });
    }
  };
}(jQuery));
