Direction Block Module 7.x
--------------------------
by sarathkm, goldenvalley4ever@gmail.com

********************************************************************************

Introduction
-------------
 This module displays a direction from A to B based on geocoder latitude and
 longitude for node content.

Required Modules
----------------
* Geofield
* GeoPHP
* Library API
* Chaos tool suite

Additional Helper Modules (optional)
------------------------------------
* Geofield Gmap

Required Library
----------------
* Maplace.js
 Download url: http://maplacejs.com/dist/maplace.min.js

Setting Up
----------
 After enabling required modules, download maplace.min.js. Place the downloaded
 js file inside libraries/maplace folder and clear cache. Now configure the
 block and place the block in appropriate region.
 If you don't have Google Maps Javascript API Key, you can get more information from 
 https://developers.google.com/maps/documentation/javascript/get-api-key.
 Note: The content being viewed should has both origin and destination
 latitude and longitude and the same must be configured
 at block configuration page.

Direction Block permissions
---------------------------
 Direction block view can be limited by roles who can be view direction by
 configuring permission from admin/people/permissions

Direction Block administration
------------------------------
 Direction block settings can be configured from block configuration page.
